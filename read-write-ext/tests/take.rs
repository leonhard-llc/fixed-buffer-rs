#![forbid(unsafe_code)]
mod test_utils;
use crate::test_utils::{err1, FakeReadWriter};
use fixed_buffer::escape_ascii;

use read_write_ext::ReadWriteExt;

#[test]
fn read_error() {
    let mut read_writer = FakeReadWriter::new(vec![Err(err1()), Ok(2), Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(
        "err1",
        std::io::Read::read(&mut take, &mut buf)
            .unwrap_err()
            .to_string()
    );
    assert_eq!(2, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(0, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("ab..", escape_ascii(&buf));
}

#[test]
fn empty() {
    let mut read_writer = FakeReadWriter::new(vec![Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(0, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("....", escape_ascii(&buf));
}

#[test]
fn doesnt_read_when_zero() {
    let mut read_writer = FakeReadWriter::empty();
    let mut take = read_writer.take_rw(0);
    let mut buf = [b'.'; 4];
    assert_eq!(0, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("....", escape_ascii(&buf));
}

#[test]
fn fewer_than_len() {
    let mut read_writer = FakeReadWriter::new(vec![Ok(2), Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(2, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(0, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("ab..", escape_ascii(&buf));
}

#[test]
fn fewer_than_len_in_multiple_reads() {
    let mut read_writer = FakeReadWriter::new(vec![Ok(2), Ok(2), Ok(0)]);
    let mut take = read_writer.take_rw(5);
    let mut buf = [b'.'; 4];
    assert_eq!(2, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(2, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("cd..", escape_ascii(&buf));
    assert_eq!(0, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("cd..", escape_ascii(&buf));
}

#[test]
fn exactly_len() {
    let mut read_writer = FakeReadWriter::new(vec![Ok(3), Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(3, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("abc.", escape_ascii(&buf));
    assert_eq!(0, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("abc.", escape_ascii(&buf));
}

#[test]
fn exactly_len_in_multiple_reads() {
    let mut read_writer = FakeReadWriter::new(vec![Ok(2), Ok(1), Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(2, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(1, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("cb..", escape_ascii(&buf));
    assert_eq!(0, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("cb..", escape_ascii(&buf));
}

#[test]
fn doesnt_call_read_after_len_reached() {
    let mut read_writer = FakeReadWriter::new(vec![Ok(3)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(3, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("abc.", escape_ascii(&buf));
    assert_eq!(0, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("abc.", escape_ascii(&buf));
}

#[test]
fn doesnt_call_read_after_len_reached_in_multiple_reads() {
    let mut read_writer = FakeReadWriter::new(vec![Ok(2), Ok(1)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(2, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(1, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("cb..", escape_ascii(&buf));
    assert_eq!(0, std::io::Read::read(&mut take, &mut buf).unwrap());
    assert_eq!("cb..", escape_ascii(&buf));
}

#[test]
fn passes_writes_through() {
    let mut read_writer = FakeReadWriter::new(vec![Ok(3)]);
    let mut take = read_writer.take_rw(2);
    assert_eq!(3, std::io::Write::write(&mut take, b"abc").unwrap());
    assert!(read_writer.is_empty());
}
