#![forbid(unsafe_code)]
mod test_utils;
use test_utils::{err1, FakeReadWriter};

use fixed_buffer::{escape_ascii, FixedBuf};
use read_write_ext::ReadWriteExt;

#[test]
fn both_empty() {
    let mut reader = std::io::Cursor::new(b"");
    let mut read_writer: FixedBuf<8> = FixedBuf::new();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 8];
    assert_eq!(0, std::io::Read::read(&mut chain, &mut buf).unwrap());
    assert_eq!("........", escape_ascii(&buf));
}

#[test]
fn doesnt_read_second_when_first_has_data() {
    let mut reader = std::io::Cursor::new(b"abc");
    let mut read_writer = FakeReadWriter::empty();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    assert_eq!(3, std::io::Read::read(&mut chain, &mut buf).unwrap());
    assert_eq!("abc.", escape_ascii(&buf));
}

#[test]
fn doesnt_read_second_when_first_returns_error() {
    let mut reader = FakeReadWriter::new(vec![Err(err1()), Err(err1())]);
    let mut read_writer = FakeReadWriter::empty();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    let err = std::io::Read::read(&mut chain, &mut buf).unwrap_err();
    assert_eq!(std::io::ErrorKind::Other, err.kind());
    assert_eq!("err1", err.to_string());
    assert_eq!("....", escape_ascii(&buf));
    std::io::Read::read(&mut chain, &mut buf).unwrap_err();
}

#[test]
fn reads_second_when_first_empty() {
    let mut reader = std::io::Cursor::new(b"");
    let mut read_writer: FixedBuf<4> = FixedBuf::new();
    read_writer.write_bytes("abc").unwrap();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    assert_eq!(3, std::io::Read::read(&mut chain, &mut buf).unwrap());
    assert_eq!("abc.", escape_ascii(&buf));
}

#[test]
fn reads_first_then_second() {
    let mut reader = std::io::Cursor::new(b"ab");
    let mut read_writer: FixedBuf<4> = FixedBuf::new();
    read_writer.write_bytes("cd").unwrap();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    assert_eq!(2, std::io::Read::read(&mut chain, &mut buf).unwrap());
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(2, std::io::Read::read(&mut chain, &mut buf).unwrap());
    assert_eq!("cd..", escape_ascii(&buf));
}

#[test]
fn returns_error_from_second() {
    let mut reader = std::io::Cursor::new(b"");
    let mut read_writer = FakeReadWriter::new(vec![Err(err1()), Err(err1())]);
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    let err = std::io::Read::read(&mut chain, &mut buf).unwrap_err();
    assert_eq!(std::io::ErrorKind::Other, err.kind());
    assert_eq!("err1", err.to_string());
    assert_eq!("....", escape_ascii(&buf));
    std::io::Read::read(&mut chain, &mut buf).unwrap_err();
}

#[test]
fn passes_writes_through() {
    let mut reader = std::io::Cursor::new(b"");
    let mut read_writer: FixedBuf<4> = FixedBuf::new();
    let mut chain = read_writer.chain_after(&mut reader);
    assert_eq!(3, std::io::Write::write(&mut chain, b"abc").unwrap());
    assert_eq!("abc", read_writer.escape_ascii());
}
