# read-write-ext

[![crates.io version](https://img.shields.io/crates/v/read-write-ext.svg)](https://crates.io/crates/read-write-ext)
[![license: Apache 2.0](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/raw/main/license-apache-2.0.svg)](http://www.apache.org/licenses/LICENSE-2.0)
[![unsafe forbidden](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/raw/main/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)
[![pipeline status](https://gitlab.com/leonhard-llc/fixed-buffer-rs/badges/main/pipeline.svg)](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/pipelines)

`ReadWriteExt` trait with `chain_after` and `take_rw` for `std::io::Read + Write` structs.

## Features
- `forbid(unsafe_code)`
- Depends only on `std`.
- Good test coverage (99%)
- Like [`std::io::Read::chain`](https://doc.rust-lang.org/std/io/trait.Read.html#method.chain)
  and [`std::io::Read::take`](https://doc.rust-lang.org/std/io/trait.Read.html#method.take)
  but also passes through writes.
- Useful with `Read + Write` objects like
  [`std::net::TcpStream`](https://doc.rust-lang.org/stable/std/net/struct.TcpStream.html)
  and [`rustls::Stream`](https://docs.rs/rustls/latest/rustls/struct.Stream.html).

## Changelog
- v0.1.1 - `take_rw` to take `u64` like `std::io::Read::take`.
- v0.1.0 - Initial release.  Moved code from `fixed-buffer`.

## TO DO

## Release Process
1. Edit `Cargo.toml` and bump version number.
1. Run `../release.sh`

License: Apache-2.0
