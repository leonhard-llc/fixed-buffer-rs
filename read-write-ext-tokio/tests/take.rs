mod test_utils;

use std::time::Duration;
use test_utils::{err1, FakeAsyncReadWriter};

use crate::test_utils::PendingAsyncReaderWriter;
use fixed_buffer::escape_ascii;
use read_write_ext_tokio::AsyncReadWriteExt;

#[tokio::test]
async fn read_error() {
    let mut read_writer = FakeAsyncReadWriter::new(vec![Err(err1()), Ok(2), Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(
        "err1",
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap_err()
            .to_string()
    );
    assert_eq!(
        2,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("ab..", escape_ascii(&buf));
}

#[tokio::test]
async fn empty() {
    let mut read_writer = FakeAsyncReadWriter::new(vec![Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("....", escape_ascii(&buf));
}

#[tokio::test]
async fn doesnt_read_when_zero() {
    let mut read_writer = FakeAsyncReadWriter::empty();
    let mut take = read_writer.take_rw(0);
    let mut buf = [b'.'; 4];
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("....", escape_ascii(&buf));
}

#[tokio::test]
async fn fewer_than_len() {
    let mut read_writer = FakeAsyncReadWriter::new(vec![Ok(2), Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(
        2,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("ab..", escape_ascii(&buf));
}

#[tokio::test]
async fn fewer_than_len_in_multiple_reads() {
    let mut read_writer = FakeAsyncReadWriter::new(vec![Ok(2), Ok(2), Ok(0)]);
    let mut take = read_writer.take_rw(5);
    let mut buf = [b'.'; 4];
    assert_eq!(
        2,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(
        2,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("cd..", escape_ascii(&buf));
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("cd..", escape_ascii(&buf));
}

#[tokio::test]
async fn exactly_len() {
    let mut read_writer = FakeAsyncReadWriter::new(vec![Ok(3), Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(
        3,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("abc.", escape_ascii(&buf));
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("abc.", escape_ascii(&buf));
}

#[tokio::test]
async fn exactly_len_in_multiple_reads() {
    let mut read_writer = FakeAsyncReadWriter::new(vec![Ok(2), Ok(1), Ok(0)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(
        2,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(
        1,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("cb..", escape_ascii(&buf));
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("cb..", escape_ascii(&buf));
}

#[tokio::test]
async fn doesnt_call_read_after_len_reached() {
    let mut read_writer = FakeAsyncReadWriter::new(vec![Ok(3)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(
        3,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("abc.", escape_ascii(&buf));
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("abc.", escape_ascii(&buf));
}

#[tokio::test]
async fn doesnt_call_read_after_len_reached_in_multiple_reads() {
    let mut read_writer = FakeAsyncReadWriter::new(vec![Ok(2), Ok(1)]);
    let mut take = read_writer.take_rw(3);
    let mut buf = [b'.'; 4];
    assert_eq!(
        2,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(
        1,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("cb..", escape_ascii(&buf));
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut take, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("cb..", escape_ascii(&buf));
}

#[tokio::test]
async fn passes_writes_through() {
    let mut read_writer = FakeAsyncReadWriter::new(vec![Ok(3)]);
    let mut take = read_writer.take_rw(2);
    assert_eq!(
        3,
        tokio::io::AsyncWriteExt::write(&mut take, b"abc")
            .await
            .unwrap()
    );
    assert!(read_writer.is_empty());
}

#[tokio::test]
async fn read_pending() {
    let mut read_writer = PendingAsyncReaderWriter {};
    let mut take = read_writer.take_rw(2);
    let mut buf = [b'.'; 4];
    tokio::time::timeout(
        Duration::from_millis(500),
        tokio::io::AsyncReadExt::read(&mut take, &mut buf),
    )
    .await
    .unwrap_err();
}
