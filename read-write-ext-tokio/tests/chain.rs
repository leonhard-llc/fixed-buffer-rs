mod test_utils;

use std::time::Duration;
use test_utils::{err1, FakeAsyncReadWriter, PendingAsyncReader};

use fixed_buffer::{escape_ascii, FixedBuf};
use read_write_ext_tokio::AsyncReadWriteExt;

#[tokio::test]
async fn both_empty() {
    let mut reader = std::io::Cursor::new(b"");
    let mut read_writer: FixedBuf<8> = FixedBuf::new();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 8];
    assert_eq!(
        0,
        tokio::io::AsyncReadExt::read(&mut chain, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("........", escape_ascii(&buf));
}

#[tokio::test]
async fn doesnt_read_second_when_first_has_data() {
    let mut reader = std::io::Cursor::new(b"abc");
    let mut read_writer = FakeAsyncReadWriter::empty();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    assert_eq!(
        3,
        tokio::io::AsyncReadExt::read(&mut chain, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("abc.", escape_ascii(&buf));
}

#[tokio::test]
async fn doesnt_read_second_when_first_is_pending() {
    let mut reader = PendingAsyncReader {};
    let mut read_writer = FakeAsyncReadWriter::empty();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    tokio::time::timeout(
        Duration::from_millis(500),
        tokio::io::AsyncReadExt::read(&mut chain, &mut buf),
    )
    .await
    .unwrap_err();
    assert_eq!("....", escape_ascii(&buf));
}

#[tokio::test]
async fn doesnt_read_second_when_first_returns_error() {
    let mut reader = FakeAsyncReadWriter::new(vec![Err(err1()), Err(err1())]);
    let mut read_writer = FakeAsyncReadWriter::empty();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    let err = tokio::io::AsyncReadExt::read(&mut chain, &mut buf)
        .await
        .unwrap_err();
    assert_eq!(std::io::ErrorKind::Other, err.kind());
    assert_eq!("err1", err.to_string());
    assert_eq!("....", escape_ascii(&buf));
    tokio::io::AsyncReadExt::read(&mut chain, &mut buf)
        .await
        .unwrap_err();
}

#[tokio::test]
async fn reads_second_when_first_empty() {
    let mut reader = std::io::Cursor::new(b"");
    let mut read_writer: FixedBuf<4> = FixedBuf::new();
    read_writer.write_bytes("abc").unwrap();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    assert_eq!(
        3,
        tokio::io::AsyncReadExt::read(&mut chain, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("abc.", escape_ascii(&buf));
}

#[tokio::test]
async fn reads_first_then_second() {
    let mut reader = std::io::Cursor::new(b"ab");
    let mut read_writer: FixedBuf<4> = FixedBuf::new();
    read_writer.write_bytes("cd").unwrap();
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    assert_eq!(
        2,
        tokio::io::AsyncReadExt::read(&mut chain, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("ab..", escape_ascii(&buf));
    assert_eq!(
        2,
        tokio::io::AsyncReadExt::read(&mut chain, &mut buf)
            .await
            .unwrap()
    );
    assert_eq!("cd..", escape_ascii(&buf));
}

#[tokio::test]
async fn returns_error_from_second() {
    let mut reader = std::io::Cursor::new(b"");
    let mut read_writer = FakeAsyncReadWriter::new(vec![Err(err1()), Err(err1())]);
    let mut chain = read_writer.chain_after(&mut reader);
    let mut buf = [b'.'; 4];
    let err = tokio::io::AsyncReadExt::read(&mut chain, &mut buf)
        .await
        .unwrap_err();
    assert_eq!(std::io::ErrorKind::Other, err.kind());
    assert_eq!("err1", err.to_string());
    assert_eq!("....", escape_ascii(&buf));
    tokio::io::AsyncReadExt::read(&mut chain, &mut buf)
        .await
        .unwrap_err();
}

#[tokio::test]
async fn passes_writes_through() {
    let mut reader = std::io::Cursor::new(b"");
    let mut read_writer: FixedBuf<4> = FixedBuf::new();
    let mut chain = read_writer.chain_after(&mut reader);
    assert_eq!(
        3,
        tokio::io::AsyncWriteExt::write(&mut chain, b"abc")
            .await
            .unwrap()
    );
    assert_eq!("abc", read_writer.escape_ascii());
}
