# read-write-ext-tokio

[![crates.io version](https://img.shields.io/crates/v/read-write-ext-tokio.svg)](https://crates.io/crates/read-write-ext-tokio)
[![license: Apache 2.0](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/raw/main/license-apache-2.0.svg)](http://www.apache.org/licenses/LICENSE-2.0)
[![unsafe forbidden](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/raw/main/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)
[![pipeline status](https://gitlab.com/leonhard-llc/fixed-buffer-rs/badges/main/pipeline.svg)](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/pipelines)

`AsyncReadWriteExt` trait with `chain_after` and `take_rw` for `tokio::io::Read + Write` structs.

## Features
- `forbid(unsafe_code)`
- Good test coverage (100%)
- Like [`tokio::io::AsyncReadExt::chain`](https://docs.rs/tokio/latest/tokio/io/trait.AsyncReadExt.html#method.chain)
  and [`tokio::io::AsyncReadExt::take`](https://docs.rs/tokio/latest/tokio/io/trait.AsyncReadExt.html#method.take)
  but also passes through writes.
- Useful with `Read + Write` objects like
  [`tokio::net::TcpStream`](https://docs.rs/tokio/latest/tokio/net/struct.TcpStream.html)
  and [`tokio_rustls::TlsStream`](https://docs.rs/tokio-rustls/latest/tokio_rustls/enum.TlsStream.html).

## Changelog
- v0.1.0 - Initial release.  Moved code from `fixed-buffer-tokio`.

## TO DO

## Release Process
1. Edit `Cargo.toml` and bump version number.
1. Run `../release.sh`

License: Apache-2.0
