use fixed_buffer::deframe_line;

#[test]
fn test_dataframe_line() {
    assert_eq!((0, None), deframe_line(b"").unwrap());
    assert_eq!((0, None), deframe_line(b"abc").unwrap());
    assert_eq!((0, None), deframe_line(b"abc\r").unwrap());
    assert_eq!((1, Some(0..0)), deframe_line(b"\n").unwrap());
    assert_eq!((2, Some(0..0)), deframe_line(b"\r\n").unwrap());
    assert_eq!((4, Some(0..3)), deframe_line(b"abc\n").unwrap());
    assert_eq!((5, Some(0..3)), deframe_line(b"abc\r\n").unwrap());
    assert_eq!((4, Some(0..3)), deframe_line(b"abc\ndef").unwrap());
    assert_eq!((5, Some(0..3)), deframe_line(b"abc\r\ndef").unwrap());
    assert_eq!((4, Some(0..3)), deframe_line(b"abc\ndef\n").unwrap());
    assert_eq!((5, Some(0..3)), deframe_line(b"abc\r\ndef\r\n").unwrap());
    assert_eq!((6, Some(0..4)), deframe_line(b"abc\r\r\n").unwrap());
}
