use fixed_buffer::FixedBuf;
use safina_async_test::async_test;
use smol::io::{AsyncReadExt, AsyncWriteExt};

#[async_test]
async fn test_read() {
    assert_eq!(0, FixedBuf::from(*b"").read(&mut [b'x'; 0]).await.unwrap());
    {
        let mut buf = FixedBuf::from(*b"a");
        assert_eq!(0, buf.read(&mut [b'x'; 0]).await.unwrap());
        assert_eq!(1, buf.len());
    }
    {
        let mut data = [b'x'; 1];
        let mut buf = FixedBuf::from(*b"");
        assert_eq!(0, buf.read(&mut data).await.unwrap());
        assert_eq!(0, buf.len());
        assert_eq!(&data, b"x");
    }
    {
        let mut data = [b'x'; 1];
        let mut buf = FixedBuf::from(*b"a");
        assert_eq!(1, buf.read(&mut data).await.unwrap());
        assert_eq!(0, buf.len());
        assert_eq!(&data, b"a");
    }
    {
        let mut data = [b'x'; 1];
        let mut buf = FixedBuf::from(*b"ab");
        assert_eq!(1, buf.read(&mut data).await.unwrap());
        assert_eq!(1, buf.len());
        assert_eq!(&data, b"a");
    }
    {
        let mut data = [b'x'; 2];
        let mut buf = FixedBuf::from(*b"a");
        assert_eq!(1, buf.read(&mut data).await.unwrap());
        assert_eq!(0, buf.len());
        assert_eq!(&data, b"ax");
    }
    {
        let mut data = [b'x'; 2];
        let mut buf = FixedBuf::from(*b"ab");
        assert_eq!(2, buf.read(&mut data).await.unwrap());
        assert_eq!(0, buf.len());
        assert_eq!(&data, b"ab");
    }
    {
        let mut data = [b'x'; 2];
        let mut buf = FixedBuf::from(*b"abc");
        assert_eq!(2, buf.read(&mut data).await.unwrap());
        assert_eq!(1, buf.len());
        assert_eq!(&data, b"ab");
    }
}

#[async_test]
async fn test_write() {
    let mut buf: FixedBuf<3> = FixedBuf::new();
    assert_eq!(2, buf.write(b"ab").await.unwrap());
    assert_eq!(1, buf.write(b"cd").await.unwrap()); // Fills buffer, "d" not written.
    assert_eq!("abc", buf.escape_ascii());
    buf.write(b"d").await.unwrap_err(); // Error, buffer is full.
}
