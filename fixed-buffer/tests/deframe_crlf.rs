use fixed_buffer::deframe_crlf;

#[test]
fn test_dataframe_crlf() {
    assert_eq!((0, None), deframe_crlf(b"").unwrap());
    assert_eq!((0, None), deframe_crlf(b"abc").unwrap());
    assert_eq!((0, None), deframe_crlf(b"abc\r").unwrap());
    assert_eq!((0, None), deframe_crlf(b"abc\n").unwrap());
    assert_eq!((2, Some(0..0)), deframe_crlf(b"\r\n").unwrap());
    assert_eq!((5, Some(0..3)), deframe_crlf(b"abc\r\n").unwrap());
    assert_eq!((5, Some(0..3)), deframe_crlf(b"abc\r\ndef").unwrap());
    assert_eq!((5, Some(0..3)), deframe_crlf(b"abc\r\ndef\r\n").unwrap());
    assert_eq!((6, Some(0..4)), deframe_crlf(b"abc\n\r\n").unwrap());
}
