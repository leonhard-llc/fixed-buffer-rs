use crate::FixedBuf;
use std::io::Error;
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::io::ReadBuf;

impl<const SIZE: usize> tokio::io::AsyncRead for FixedBuf<SIZE> {
    fn poll_read(
        mut self: Pin<&mut Self>,
        _cx: &mut Context<'_>,
        read_buf: &mut ReadBuf<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        let len = self.len().min(read_buf.remaining());
        read_buf.put_slice(self.try_read_exact(len).unwrap());
        Poll::Ready(Ok(()))
    }
}

impl<const SIZE: usize> tokio::io::AsyncWrite for FixedBuf<SIZE> {
    fn poll_write(
        mut self: Pin<&mut Self>,
        _cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, Error>> {
        Poll::Ready(Ok(self.write_bytes(buf)?))
    }

    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<(), Error>> {
        Poll::Ready(Ok(()))
    }

    fn poll_shutdown(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<(), Error>> {
        Poll::Ready(Ok(()))
    }
}
