use super::MalformedInputError;

/// Deframes lines that are terminated by either `b'\n'` or `b"\r\n"`.
///
/// See [`read_frame`](struct.FixedBuf.html#method.read_frame).
///
/// # Errors
/// Returns an error when `data` does not contain `b'\n'` or `b"\r\n"`.
pub fn deframe_line(
    data: &[u8],
) -> Result<(usize, Option<core::ops::Range<usize>>), MalformedInputError> {
    for n in 0..data.len() {
        if data[n] == b'\n' {
            let payload_start_incl = 0;
            let payload_end_excl = if n > 0 && data[n - 1] == b'\r' {
                n - 1
            } else {
                n
            };
            let payload_range = payload_start_incl..payload_end_excl;
            let frame_len = n + 1;
            return Ok((frame_len, Some(payload_range)));
        }
    }
    Ok((0, None))
}
