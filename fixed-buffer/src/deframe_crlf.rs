use super::MalformedInputError;

/// Deframes lines that are terminated by `b"\r\n"`.
/// Ignores solitary `b'\n'`.
///
/// See [`read_frame`](struct.FixedBuf.html#method.read_frame).
///
/// # Errors
/// Returns an error when `data` does not contain `b"\r\n"`.
pub fn deframe_crlf(
    data: &[u8],
) -> Result<(usize, Option<core::ops::Range<usize>>), MalformedInputError> {
    if data.len() > 1 {
        for n in 1..data.len() {
            if data[n - 1] == b'\r' && data[n] == b'\n' {
                let payload_range = 0..(n - 1);
                let frame_len = n + 1;
                return Ok((frame_len, Some(payload_range)));
            }
        }
    }
    Ok((0, None))
}
