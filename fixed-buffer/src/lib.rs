//! # fixed-buffer
//! [![crates.io version](https://img.shields.io/crates/v/fixed-buffer.svg)](https://crates.io/crates/fixed-buffer)
//! [![license: Apache 2.0](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/raw/main/license-apache-2.0.svg)](http://www.apache.org/licenses/LICENSE-2.0)
//! [![unsafe forbidden](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/raw/main/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)
//! [![pipeline status](https://gitlab.com/leonhard-llc/fixed-buffer-rs/badges/main/pipeline.svg)](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/pipelines)
//!
//! This is a Rust library with fixed-size buffers,
//! useful for network protocol parsers and file parsers.
//!
//! # Features
//! - `forbid(unsafe_code)`
//! - Depends only on `std`
//! - Write bytes to the buffer and read them back
//! - Lives on the stack
//! - Does not allocate
//! - Use it to read a stream, search for a delimiter,
//!   and save leftover bytes for the next read.
//! - No macros
//! - Good test coverage (100%)
//! - Async support by enabling cargo features `async-std-feature`, `futures-io`, `smol-feature`, or `tokio`.
//!
//! # Limitations
//! - Not a circular buffer.
//!   You can call `shift()` periodically
//!   to move unread bytes to the front of the buffer.
//!
//! # Examples
//! Read and handle requests from a remote client:
//! ```rust
//! # struct Request {}
//! # impl Request {
//! #     pub fn parse(line_bytes: &[u8]) -> Result<Request, std::io::Error> {
//! #         Ok(Request{})
//! #     }
//! # }
//! # fn handle_request(request: Request) -> Result<(), std::io::Error> {
//! #     Ok(())
//! # }
//! use fixed_buffer::{deframe_line, FixedBuf};
//! use std::io::Error;
//! use std::net::TcpStream;
//!
//! fn handle_conn(mut tcp_stream: TcpStream) -> Result<(), Error> {
//!     let mut buf: FixedBuf<4096> = FixedBuf::new();
//!     loop {
//!         // Read a line
//!         // and leave leftover bytes in `buf`.
//!         let line_bytes = match buf.read_frame(
//!             &mut tcp_stream, deframe_line)? {
//!                 Some(line_bytes) => line_bytes,
//!                 None => return Ok(()),
//!             };
//!         let request = Request::parse(line_bytes)?;
//!         handle_request(request)?;
//!     }
//! }
//! ```
//! For a complete example, see
//! [`tests/server.rs`](https://gitlab.com/leonhard-llc/fixed-buffer-rs/-/blob/main/fixed-buffer/tests/server.rs).
//!
//! Read and process records:
//! ```rust
//! use fixed_buffer::FixedBuf;
//! use std::io::{Error, ErrorKind, Read};
//! use std::net::TcpStream;
//!
//! fn try_process_record(b: &[u8]) -> Result<usize, Error> {
//!     if b.len() < 2 {
//!         return Ok(0);
//!     }
//!     if b.starts_with("ab".as_bytes()) {
//!         println!("found record");
//!         Ok(2)
//!     } else {
//!         Err(Error::new(ErrorKind::InvalidData, "bad record"))
//!     }
//! }
//!
//! fn read_and_process<R: Read>(mut input: R) -> Result<(), Error> {
//!     let mut buf: FixedBuf<1024> = FixedBuf::new();
//!     loop {
//!         // Read a chunk into the buffer.
//!         if buf.copy_once_from(&mut input)? == 0 {
//!             return if buf.len() == 0 {
//!                 // EOF at record boundary
//!                 Ok(())
//!             } else {
//!                 // EOF in the middle of a record
//!                 Err(Error::from(
//!                     ErrorKind::UnexpectedEof))
//!             };
//!         }
//!         // Process records in the buffer.
//!         loop {
//!             let num_to_consume =
//!                 try_process_record(buf.readable())?;
//!             if num_to_consume == 0 {
//!                 break;
//!             }
//!             buf.try_read_exact(num_to_consume).unwrap();
//!         }
//!         // Shift data in the buffer to free up
//!         // space at the end for writing.
//!         buf.shift();
//!     }
//! }
//! #
//! # fn main() {
//! #     read_and_process(std::io::Cursor::new(b"abab")).unwrap();
//! #     read_and_process(std::io::Cursor::new(b"ababc")).unwrap_err();
//! # }
//! ```
//!
//! The `From<&[u8; SIZE]>` implementation is useful in tests.  Example:
//! ```
//! # use fixed_buffer::FixedBuf;
//! use core::convert::From;
//! assert_eq!(3, FixedBuf::from(b"abc").len());
//! ```
//!
//! # Cargo Geiger Safety Report
//! # Alternatives
//! - [`bytes`](https://crates.io/crates/bytes), lots of [`unsafe`](https://github.com/search?q=repo%3Atokio-rs%2Fbytes+unsafe+path%3Asrc%2F**&type=code)
//! - [`buf_redux`](https://crates.io/crates/buf_redux), circular buffer support, updated in 2019
//! - [`std::io::BufReader`](https://doc.rust-lang.org/std/io/struct.BufReader.html)
//! - [`std::io::BufWriter`](https://doc.rust-lang.org/std/io/struct.BufWriter.html)
//! - [`static-buffer`](https://crates.io/crates/static-buffer), updated in 2016
//! - [`block-buffer`](https://crates.io/crates/block-buffer), for processing fixed-length blocks of data, some [`unsafe`](https://github.com/search?q=repo%3ARustCrypto%2Futils+unsafe+path%3Ablock-buffer%2F**&type=code)
//! - [`arrayvec`](https://crates.io/crates/arrayvec), vector with fixed capacity, some [`unsafe`](https://github.com/search?q=repo%3Abluss%2Farrayvec+unsafe+path%3Asrc%2F**&type=code)
//!
//! # Changelog
//! - v1.0.0 2024-10-20 - `From<&[u8; SIZE]>`, `FixedBuffer::from(b"x")`
//! - v0.5.0 2022-03-21 - Move `ReadWriteChain` and `ReadWriteTake` to new
//!   [`read-write-ext`](https://crates.io/crates/read-write-ext) crate.
//! - v0.4.0 2022-03-21
//!   - `From<[u8; SIZE]>`, `FixedBuffer::from([0])`
//!   - `write_bytes` to take `AsRef<[u8]>`
//!   - Rename `try_read_exact` to `read_and_copy_exact`.
//!   - Rename `try_read_bytes` to `try_read_exact`.
//!   - Remove `empty`, `filled`, `read_byte`, `read_bytes`, `try_parse`, and `write_str`.
//!   - `deframe` to allow consuming bytes without returning a frame
//!   - `write_bytes` to write as many bytes as it can,
//!     and return new `NoWritableSpace` error only when it cannot write any bytes.
//!     Remove `NotEnoughSpaceError`.  The method now behaves like `std::io::Write::write`.
//!
//! <details>
//! <summary>Older changelog entries</summary>
//!
//! - v0.3.1 - Implement `From<NotEnoughSpaceError>` and `From<MalformedInputError>` for `String`.
//! - v0.3.0 - Breaking API changes:
//!   - Change type parameter to const buffer size. Example: `FixedBuf<1024>`.
//!   - Remove `new` arg.
//!   - Remove `capacity`.
//!   - Remove `Copy` impl.
//!   - Change `writable` return type to `&mut [u8]`.
//! - v0.2.3
//!   - Add
//!     [`read_byte`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.read_byte),
//!     [`try_read_byte`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.try_read_byte),
//!     [`try_read_bytes`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.try_read_bytes),
//!     [`try_read_exact`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.try_read_exact),
//!     [`try_parse`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.try_parse).
//!   - Implement [`UnwindSafe`](https://doc.rust-lang.org/std/panic/trait.UnwindSafe.html)
//! - v0.2.2 - Add badges to readme
//! - v0.2.1 - Add
//!   [`deframe`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.deframe)
//!   and
//!   [`mem`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.mem),
//!   needed by `AsyncFixedBuf::read_frame`.
//! - v0.2.0
//!   - Move tokio support to [`fixed_buffer_tokio`](https://crates.io/crates/fixed-buffer-tokio).
//!   - Add
//!     [`copy_once_from`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.copy_once_from),
//!     [`read_block`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.read_block),
//!     [`ReadWriteChain`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.ReadWriteChain.html),
//!     and
//!     [`ReadWriteTake`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.ReadWriteTake.html).
//! - v0.1.7 - Add [`FixedBuf::escape_ascii`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.escape_ascii).
//! - v0.1.6 - Add [`filled`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.filled)
//!   constructor.
//! - v0.1.5 - Change [`read_delimited`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.read_delimited)
//!   to return `Option<&[u8]>`, for clean EOF handling.
//! - v0.1.4 - Add [`clear()`](https://docs.rs/fixed-buffer/latest/fixed_buffer/struct.FixedBuf.html#method.clear).
//! - v0.1.3
//!   - Thanks to [freax13](https://gitlab.com/Freax13) for these changes:
//!     - Support any buffer size.  Now you can make `FixedBuf<[u8; 42]>`.
//!     - Support any `AsRef<[u8]> + AsMut<[u8]>` value for internal memory:
//!       - `[u8; N]`
//!       - `Box<[u8; N]>`
//!       - `&mut [u8]`
//!       - `Vec<u8>`
//!   - Renamed `new_with_mem` to `new`.
//!     Use `FixedBuf::default()` to construct any `FixedBuf<T: Default>`, which includes
//!     [arrays of sizes up to 32](https://doc.rust-lang.org/std/primitive.array.html).
//! - v0.1.2 - Updated documentation.
//! - v0.1.1 - First published version
//!
//! </details>
//!
//! # TO DO
//! - Change links in docs to standard style.  Don't link to `docs.rs`.
//! - Idea: `buf.slice(buf.read_frame(&mut reader, deframe_crlf))`
//! - Add an `frame_copy_iter` function.
//!   Because of borrowing rules, this function must return non-borrowed (allocated and copied) data.
//! - Set up CI on:
//!   - DONE - Linux x86 64-bit
//!   - [macOS](https://gitlab.com/gitlab-org/gitlab/-/issues/269756)
//!   - [Windows](https://about.gitlab.com/blog/2020/01/21/windows-shared-runner-beta/)
//!   - <https://crate-ci.github.io/pr/testing.html#travisci>
//!   - Linux ARM 64-bit (Raspberry Pi 3 and newer)
//!   - Linux ARM 32-bit (Raspberry Pi 2)
//!   - RISCV & ESP32 firmware?
//! - DONE - Try to make this crate comply with the [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/).
//! - DONE - Find out how to include Readme.md info in the crate's docs.
//! - DONE - Make the repo public
//! - DONE - Set up continuous integration tests and banner.
//!   - <https://github.com/actions-rs/example>
//!   - <https://alican.codes/rust-github-actions/>
//! - DONE - Add some documentation tests
//!   - <https://doc.rust-lang.org/rustdoc/documentation-tests.html>
//!   - <https://doc.rust-lang.org/stable/rust-by-example/testing/doc_testing.html>
//! - DONE - Set up public repository on Gitlab.com
//!   - <https://gitlab.com/mattdark/firebase-example/blob/master/.gitlab-ci.yml>
//!   - <https://medium.com/astraol/optimizing-ci-cd-pipeline-for-rust-projects-gitlab-docker-98df64ae3bc4>
//!   - <https://hub.docker.com/_/rust>
//! - DONE - Publish to crates.io
//! - DONE - Read through <https://crate-ci.github.io/index.html>
//! - DONE - Get a code review from an experienced rustacean
//! - DONE - Add and update a changelog
//!   - Update it manually
//!   - <https://crate-ci.github.io/release/changelog.html>
//!
//! # Release Process
//! 1. Edit `Cargo.toml` and bump version number.
//! 1. Run `../release.sh`
#![forbid(unsafe_code)]

mod escape_ascii;
pub use escape_ascii::escape_ascii;

mod deframe_crlf;
pub use deframe_crlf::deframe_crlf;

mod deframe_line;
pub use deframe_line::deframe_line;

#[cfg(feature = "futures-io")]
mod impl_futures_io;
#[cfg(feature = "futures-io")]
#[allow(unused_imports)]
pub use impl_futures_io::*;

#[cfg(feature = "tokio")]
mod impl_tokio;
#[cfg(feature = "tokio")]
#[allow(unused_imports)]
pub use impl_tokio::*;

#[derive(Copy, Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct NoWritableSpace {}
impl From<NoWritableSpace> for std::io::Error {
    fn from(_: NoWritableSpace) -> Self {
        std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "no writable space in buffer",
        )
    }
}
impl From<NoWritableSpace> for String {
    fn from(_: NoWritableSpace) -> Self {
        "no writable space in buffer".to_string()
    }
}

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct MalformedInputError(pub String);
impl MalformedInputError {
    #[must_use]
    pub fn new(msg: String) -> Self {
        Self(msg)
    }
}
impl From<MalformedInputError> for std::io::Error {
    fn from(e: MalformedInputError) -> Self {
        std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            format!("malformed input: {}", e.0),
        )
    }
}
impl From<MalformedInputError> for String {
    fn from(e: MalformedInputError) -> Self {
        format!("malformed input: {}", e.0)
    }
}

/// A fixed-length byte buffer.
/// You can write bytes to it and then read them back.
///
/// It is not a circular buffer.  Call [`shift`](#method.shift) periodically to
/// move unread bytes to the front of the buffer.
#[derive(Copy, Clone, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct FixedBuf<const SIZE: usize> {
    mem: [u8; SIZE],
    read_index: usize,
    write_index: usize,
}

impl<const SIZE: usize> std::panic::UnwindSafe for FixedBuf<SIZE> {}

impl<const SIZE: usize> FixedBuf<SIZE> {
    /// Makes a new empty buffer with space for `SIZE` bytes.
    ///
    /// Be careful of stack overflows!
    #[must_use]
    pub const fn new() -> Self {
        Self {
            mem: [0_u8; SIZE],
            write_index: 0,
            read_index: 0,
        }
    }

    /// Drops the struct and returns its internal array.
    #[must_use]
    pub fn into_inner(self) -> [u8; SIZE] {
        self.mem
    }

    /// Returns the number of unread bytes in the buffer.
    ///
    /// # Example
    /// ```
    /// # use fixed_buffer::FixedBuf;
    /// let mut buf: FixedBuf<16> = FixedBuf::new();
    /// assert_eq!(0, buf.len());
    /// buf.write_bytes("abc");
    /// assert_eq!(3, buf.len());
    /// buf.try_read_exact(2).unwrap();
    /// assert_eq!(1, buf.len());
    /// buf.shift();
    /// assert_eq!(1, buf.len());
    /// buf.read_all();
    /// assert_eq!(0, buf.len());
    /// ```
    #[must_use]
    pub fn len(&self) -> usize {
        self.write_index - self.read_index
    }

    /// Returns true if there are unread bytes in the buffer.
    ///
    /// # Example
    /// ```
    /// # use fixed_buffer::FixedBuf;
    /// let mut buf: FixedBuf<16> = FixedBuf::new();
    /// assert!(buf.is_empty());
    /// buf.write_bytes("abc").unwrap();
    /// assert!(!buf.is_empty());
    /// buf.read_all();
    /// assert!(buf.is_empty());
    /// ```
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.write_index == self.read_index
    }

    /// Discards all data in the buffer.
    pub fn clear(&mut self) {
        self.read_index = 0;
        self.write_index = 0;
    }

    /// Copies all readable bytes to a string.
    /// Includes printable ASCII characters as-is.
    /// Converts non-printable characters to strings like "\n" and "\x19".
    ///
    /// Uses
    /// [`core::ascii::escape_default`](https://doc.rust-lang.org/core/ascii/fn.escape_default.html)
    /// internally to escape each byte.
    ///
    /// This function is useful for printing byte slices to logs and comparing byte slices in tests.
    ///
    /// Example test:
    /// ```
    /// use fixed_buffer::FixedBuf;
    /// let mut buf: FixedBuf<8> = FixedBuf::new();
    /// buf.write_bytes("abc");
    /// buf.write_bytes("€");
    /// assert_eq!("abc\\xe2\\x82\\xac", buf.escape_ascii());
    /// ```
    #[must_use]
    pub fn escape_ascii(&self) -> String {
        escape_ascii(self.readable())
    }

    /// Borrows the entire internal memory buffer.
    /// This is a low-level function.
    #[must_use]
    pub fn mem(&self) -> &[u8] {
        self.mem.as_ref()
    }

    /// Returns the slice of readable bytes in the buffer.
    /// After processing some bytes from the front of the slice,
    /// call [`read`](#method.read) to consume the bytes.
    ///
    /// This is a low-level method.
    /// You probably want to use
    /// [`std::io::Read::read`](https://doc.rust-lang.org/std/io/trait.Read.html#tymethod.read)
    /// or
    /// [`tokio::io::AsyncReadExt::read`](https://docs.rs/tokio/0.3.0/tokio/io/trait.AsyncReadExt.html#method.reade).
    #[must_use]
    pub fn readable(&self) -> &[u8] {
        &self.mem.as_ref()[self.read_index..self.write_index]
    }

    /// Reads a single byte from the buffer.
    ///
    /// Returns `None` if the buffer is empty.
    pub fn try_read_byte(&mut self) -> Option<u8> {
        self.try_read_exact(1).map(|bytes| bytes[0])
    }

    /// Reads bytes from the buffer.
    ///
    /// Returns `None` if the buffer does not contain `num_bytes` bytes.
    pub fn try_read_exact(&mut self, num_bytes: usize) -> Option<&[u8]> {
        let new_read_index = self.read_index + num_bytes;
        if self.write_index < new_read_index {
            None
        } else {
            let old_read_index = self.read_index;
            // We update `read_index` after any possible panic.
            // This keeps the struct consistent even when a panic happens.
            // This complies with the contract of std::panic::UnwindSafe.
            self.read_index = new_read_index;
            if self.read_index == self.write_index {
                // All data has been read.  Reset the buffer.
                self.write_index = 0;
                self.read_index = 0;
            }
            Some(&self.mem.as_ref()[old_read_index..new_read_index])
        }
    }

    /// Reads all the bytes from the buffer.
    ///
    /// The buffer becomes empty and subsequent writes can fill the whole buffer.
    #[allow(clippy::missing_panics_doc)]
    pub fn read_all(&mut self) -> &[u8] {
        self.try_read_exact(self.len()).unwrap()
    }

    /// Reads bytes from the buffer and copies them into `dest`.
    ///
    /// Returns the number of bytes copied.
    ///
    /// Returns `0` when the buffer is empty or `dest` is zero-length.
    #[allow(clippy::missing_panics_doc)]
    pub fn read_and_copy_bytes(&mut self, dest: &mut [u8]) -> usize {
        let readable = self.readable();
        let len = core::cmp::min(dest.len(), readable.len());
        if len == 0 {
            return 0;
        }
        let src = &readable[..len];
        let copy_dest = &mut dest[..len];
        copy_dest.copy_from_slice(src);
        self.try_read_exact(len).unwrap();
        len
    }

    /// Reads byte from the buffer and copies them into `dest`, filling it,
    /// and returns `Some(())`.
    ///
    /// Returns `None` if the buffer does not contain enough bytes tp fill `dest`.
    ///
    /// Returns `Some(())` if `dest` is zero-length.
    #[allow(clippy::missing_panics_doc)]
    pub fn read_and_copy_exact(&mut self, dest: &mut [u8]) -> Option<()> {
        if self.len() < dest.len() {
            return None;
        }
        assert_eq!(dest.len(), self.read_and_copy_bytes(dest));
        Some(())
    }

    /// Reads from `reader` once and writes the data into the buffer.
    ///
    /// Use [`shift`](#method.shift) to make empty space usable for writing.
    ///
    /// # Errors
    /// Returns [`InvalidData`](std::io::ErrorKind::InvalidData)
    /// if there is no empty space in the buffer.
    pub fn copy_once_from(
        &mut self,
        reader: &mut impl std::io::Read,
    ) -> Result<usize, std::io::Error> {
        let writable = self.writable();
        if writable.is_empty() {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                "no empty space in buffer",
            ));
        };
        let num_read = reader.read(writable)?;
        self.wrote(num_read);
        Ok(num_read)
    }

    /// Tries to write `data` into the buffer, after any unread bytes.
    ///
    /// Returns `Ok(num_written)`, which may be less than the length of `data`.
    ///
    /// Returns `Ok(0)` only when `data` is empty.
    ///
    /// Use [`shift`](#method.shift) to make empty space usable for writing.
    ///
    /// # Errors
    /// Returns `NoWritableSpace` when the buffer has no free space at the end.
    ///
    /// # Example
    /// ```
    /// # use fixed_buffer::FixedBuf;
    /// let mut buf: FixedBuf<3> = FixedBuf::new();
    /// assert_eq!(2, buf.write_bytes("ab").unwrap());
    /// assert_eq!(1, buf.write_bytes("cd").unwrap()); // Fills buffer, "d" not written.
    /// assert_eq!("abc", buf.escape_ascii());
    /// buf.write_bytes("d").unwrap_err();  // Error, buffer is full.
    /// ```
    pub fn write_bytes(&mut self, data: impl AsRef<[u8]>) -> Result<usize, NoWritableSpace> {
        let data = data.as_ref();
        if data.is_empty() {
            return Ok(0);
        }
        let writable = self.writable();
        if writable.is_empty() {
            return Err(NoWritableSpace {});
        }
        let len = writable.len().min(data.len());
        let dest = &mut writable[..len];
        let src = &data[..len];
        dest.copy_from_slice(src);
        self.wrote(len);
        Ok(len)
    }

    /// Returns the writable part of the buffer.
    ///
    /// To use this, first modify bytes at the beginning of the slice.
    /// Then call [`wrote(usize)`](#method.wrote)
    /// to commit those bytes into the buffer and make them available for reading.
    ///
    /// Returns an empty slice when the end of the buffer is full.
    ///
    /// Use [`shift`](#method.shift) to make empty space usable for writing.
    ///
    /// This is a low-level method.
    /// You probably want to use
    /// [`std::io::Write::write`](https://doc.rust-lang.org/std/io/trait.Write.html#tymethod.write)
    /// or
    /// [`tokio::io::AsyncWriteExt::write`](https://docs.rs/tokio/0.3.0/tokio/io/trait.AsyncWriteExt.html#method.write).
    ///
    /// # Example
    /// ```
    /// # use fixed_buffer::{escape_ascii, FixedBuf};
    /// let mut buf: FixedBuf<8> = FixedBuf::new();
    /// buf.writable()[0] = 'a' as u8;
    /// buf.writable()[1] = 'b' as u8;
    /// buf.writable()[2] = 'c' as u8;
    /// buf.wrote(3);
    /// assert_eq!("abc", buf.escape_ascii());
    /// ```
    pub fn writable(&mut self) -> &mut [u8] {
        &mut self.mem.as_mut()[self.write_index..]
    }

    /// Commits bytes into the buffer.
    /// Call this after writing to the front of the
    /// [`writable`](#method.writable) slice.
    ///
    /// This is a low-level method.
    ///
    /// See [`writable()`](#method.writable).
    ///
    /// # Panics
    /// Panics when there is not `num_bytes` free at the end of the buffer.
    pub fn wrote(&mut self, num_bytes: usize) {
        if num_bytes == 0 {
            return;
        }
        let new_write_index = self.write_index + num_bytes;
        assert!(
            new_write_index <= self.mem.as_mut().len(),
            "write would overflow"
        );
        self.write_index = new_write_index;
    }

    /// Recovers buffer space.
    ///
    /// The buffer is not circular.
    /// After you read bytes, the space at the beginning of the buffer is unused.
    /// Call this method to move unread data to the beginning of the buffer and recover the space.
    /// This makes the free space available for writes, which go at the end of the buffer.
    pub fn shift(&mut self) {
        if self.read_index == 0 {
            return;
        }
        // As long as try_read_exact performs this check and is the only way to
        // advance read_index, this block can never execute.
        // if self.read_index == self.write_index {
        //     self.write_index = 0;
        //     self.read_index = 0;
        //     return;
        // }
        self.mem
            .as_mut()
            .copy_within(self.read_index..self.write_index, 0);
        self.write_index -= self.read_index;
        self.read_index = 0;
    }

    /// This is a low-level function.
    /// Use [`read_frame`](#method.read_frame) instead.
    ///
    /// Calls `deframer_fn` to check if the buffer contains a complete frame.
    /// Consumes the frame bytes from the buffer
    /// and returns the range of the frame's contents in the internal memory.
    ///
    /// Use [`mem`](#method.mem) to immutably borrow the internal memory and
    /// construct the slice with `&buf.mem()[range]`.
    /// This is necessary because `deframe` borrows `self` mutably but
    /// `read_frame` needs to borrow it immutably and return a slice.
    ///
    /// Returns `None` if the buffer is empty or contains an incomplete frame.
    ///
    /// # Errors
    /// Returns [`InvalidData`](std::io::ErrorKind::InvalidData)
    /// when `deframer_fn` returns an error.
    #[allow(clippy::missing_panics_doc)]
    pub fn deframe<F>(
        &mut self,
        deframer_fn: F,
    ) -> Result<Option<core::ops::Range<usize>>, std::io::Error>
    where
        F: Fn(&[u8]) -> Result<(usize, Option<core::ops::Range<usize>>), MalformedInputError>,
    {
        if self.is_empty() {
            return Ok(None);
        }
        let (num_to_consume, opt_data_range) = deframer_fn(self.readable())?;
        let opt_mem_range = opt_data_range.map(|data_range| {
            let mem_start = self.read_index + data_range.start;
            let mem_end = self.read_index + data_range.end;
            mem_start..mem_end
        });
        self.try_read_exact(num_to_consume).unwrap();
        Ok(opt_mem_range)
    }

    /// Reads from `reader` into the buffer.
    ///
    /// After each read, calls `deframer_fn`
    /// to check if the buffer now contains a complete frame.
    /// Consumes the frame bytes from the buffer
    /// and returns a slice with the frame contents.
    ///
    /// Returns `None` when `reader` reaches EOF and the buffer is empty.
    ///
    /// Calls [`shift`](#method.shift) before reading.
    ///
    /// Provided deframer functions:
    /// - [`deframe_line`](https://docs.rs/fixed-buffer/latest/fixed_buffer/fn.deframe_line.html)
    /// - [`deframe_crlf`](https://docs.rs/fixed-buffer/latest/fixed_buffer/fn.deframe_crlf.html)
    ///
    /// # Errors
    /// Returns [`UnexpectedEof`](std::io::ErrorKind::UnexpectedEof)
    /// when `reader` reaches EOF and the buffer contains an incomplete frame.
    ///
    /// Returns [`InvalidData`](std::io::ErrorKind::InvalidData)
    /// when `deframer_fn` returns an error or the buffer fills up.
    ///
    /// # Example
    /// ```
    /// # use fixed_buffer::{escape_ascii, FixedBuf, deframe_line};
    /// let mut buf: FixedBuf<32> = FixedBuf::new();
    /// let mut input = std::io::Cursor::new(b"aaa\r\nbbb\n\nccc\n");
    /// # let mut output: Vec<String> = Vec::new();
    /// loop {
    ///   if let Some(line) = buf.read_frame(&mut input, deframe_line).unwrap() {
    ///     println!("{}", escape_ascii(line));
    /// #   output.push(escape_ascii(line));
    ///   } else {
    ///     // EOF.
    ///     break;
    ///   }
    /// }
    /// // Prints:
    /// // aaa
    /// // bbb
    /// //
    /// // ccc
    /// # assert_eq!(
    /// #     vec!["aaa".to_string(), "bbb".to_string(),"".to_string(), "ccc".to_string()],
    /// #     output
    /// # );
    /// ```
    ///
    /// # Deframer Function `deframe_fn`
    /// Checks if `data` contains an entire frame.
    ///
    /// Never panics.
    ///
    /// Returns `Ok((frame_len, Some(payload_range))`
    /// when `data` contains a complete frame at `&data[payload_range]`.
    /// The caller should consume `frame_len` from the beginning of the buffer
    /// before calling `deframe` again.
    ///
    /// Returns `Ok((frame_len, None))` if `data` contains an incomplete frame.
    /// The caller should consume `frame_len` from the beginning of the buffer.
    /// The caller can read more bytes and call `deframe` again.
    ///
    /// Returns `Err(MalformedInputError)` if `data` contains a malformed frame.
    ///
    /// Popular frame formats:
    /// - Newline-delimited: CSV, JSONL, HTTP, Server-Sent Events `text/event-stream`, and SMTP
    /// - Hexadecimal length prefix: [HTTP chunked transfer encoding](https://tools.ietf.org/html/rfc7230#section-4.1)
    /// - Binary length prefix: [TLS](https://tools.ietf.org/html/rfc5246#section-6.2.1)
    ///
    /// # Example
    /// ```
    /// use fixed_buffer::deframe_crlf;
    /// assert_eq!(Ok((0, None)), deframe_crlf(b""));
    /// assert_eq!(Ok((0, None)), deframe_crlf(b"abc"));
    /// assert_eq!(Ok((0, None)), deframe_crlf(b"abc\r"));
    /// assert_eq!(Ok((0, None)), deframe_crlf(b"abc\n"));
    /// assert_eq!(Ok((5, Some((0..3)))), deframe_crlf(b"abc\r\n"));
    /// assert_eq!(Ok((5, Some((0..3)))), deframe_crlf(b"abc\r\nX"));
    /// ```
    pub fn read_frame<R, F>(
        &mut self,
        reader: &mut R,
        deframer_fn: F,
    ) -> Result<Option<&[u8]>, std::io::Error>
    where
        R: std::io::Read,
        F: Fn(&[u8]) -> Result<(usize, Option<core::ops::Range<usize>>), MalformedInputError>,
    {
        loop {
            if !self.is_empty() {
                if let Some(frame_range) = self.deframe(&deframer_fn)? {
                    return Ok(Some(&self.mem()[frame_range]));
                }
                // None case falls through.
            }
            self.shift();
            let writable = self.writable();
            if writable.is_empty() {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "end of buffer full",
                ));
            };
            let num_read = reader.read(writable)?;
            if num_read == 0 {
                if self.is_empty() {
                    return Ok(None);
                }
                return Err(std::io::Error::new(
                    std::io::ErrorKind::UnexpectedEof,
                    "eof after reading part of a frame",
                ));
            }
            self.wrote(num_read);
        }
    }
}

impl<const SIZE: usize> From<[u8; SIZE]> for FixedBuf<SIZE> {
    fn from(mem: [u8; SIZE]) -> Self {
        Self {
            mem,
            read_index: 0,
            write_index: SIZE,
        }
    }
}

impl<const SIZE: usize> From<&[u8; SIZE]> for FixedBuf<SIZE> {
    fn from(mem: &[u8; SIZE]) -> Self {
        Self {
            mem: *mem,
            read_index: 0,
            write_index: SIZE,
        }
    }
}

impl<const SIZE: usize> std::io::Write for FixedBuf<SIZE> {
    fn write(&mut self, data: &[u8]) -> Result<usize, std::io::Error> {
        Ok(self.write_bytes(data)?)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

impl<const SIZE: usize> std::io::Read for FixedBuf<SIZE> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        Ok(self.read_and_copy_bytes(buf))
    }
}

impl<const SIZE: usize> core::fmt::Debug for FixedBuf<SIZE> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
        write!(
            f,
            "FixedBuf<{}>{{{} writable, {} readable: \"{}\"}}",
            SIZE,
            SIZE - self.write_index,
            self.len(),
            self.escape_ascii()
        )
    }
}

impl<const SIZE: usize> Default for FixedBuf<SIZE> {
    fn default() -> Self {
        Self::new()
    }
}
