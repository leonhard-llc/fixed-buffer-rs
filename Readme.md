Rust libraries with fixed-size buffers,
useful for network protocol parsers and file parsers.
